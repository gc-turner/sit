-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2017 at 02:57 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sit`
--
CREATE DATABASE IF NOT EXISTS `sit` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `sit`;

-- --------------------------------------------------------

--
-- Table structure for table `curso`
--

CREATE TABLE `curso` (
  `id` int(5) NOT NULL,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `categoria` varchar(50) COLLATE utf8_bin NOT NULL,
  `pchave` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `instrutor` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `curso_atividade`
--

CREATE TABLE `curso_atividade` (
  `id` int(5) NOT NULL,
  `id_unidade` int(5) NOT NULL COMMENT 'fk curso_unidade(id)',
  `nome` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `curso_material`
--

CREATE TABLE `curso_material` (
  `id` int(5) NOT NULL,
  `id_curso` int(5) NOT NULL COMMENT 'fk curso(id)',
  `nome` varchar(60) COLLATE utf8_bin NOT NULL,
  `arquivo` mediumblob NOT NULL,
  `tipo` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `curso_questao`
--

CREATE TABLE `curso_questao` (
  `id` int(5) NOT NULL,
  `id_atividade` int(5) NOT NULL COMMENT 'fk curso_atividade(id)',
  `questao` text COLLATE utf8_bin NOT NULL,
  `alt1` text COLLATE utf8_bin NOT NULL,
  `alt2` text COLLATE utf8_bin NOT NULL,
  `alt3` text COLLATE utf8_bin NOT NULL,
  `alt4` text COLLATE utf8_bin NOT NULL,
  `resp` enum('1','2','3','4') COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `curso_unidade`
--

CREATE TABLE `curso_unidade` (
  `id` int(5) NOT NULL,
  `id_curso` int(5) NOT NULL COMMENT 'fk curso(id)',
  `nome` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `curso_video`
--

CREATE TABLE `curso_video` (
  `id` int(5) NOT NULL,
  `id_unidade` int(5) NOT NULL COMMENT 'fk unidade(id)',
  `nome` varchar(60) COLLATE utf8_bin NOT NULL,
  `externo` tinyint(4) NOT NULL DEFAULT '0',
  `link` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `plataforma` enum('youtube','vimeo','metacafe','veoh') COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(5) NOT NULL,
  `uid` int(5) NOT NULL,
  `id_curso` int(5) NOT NULL,
  `topico` varchar(150) COLLATE utf8_bin NOT NULL,
  `msg` text COLLATE utf8_bin NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `forum_msg`
--

CREATE TABLE `forum_msg` (
  `id` int(5) NOT NULL,
  `id_forum` int(5) NOT NULL,
  `uid` int(5) NOT NULL,
  `msg` text COLLATE utf8_bin NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `uid` int(5) NOT NULL,
  `uname` varchar(10) COLLATE utf8_bin NOT NULL,
  `upass` varchar(255) COLLATE utf8_bin NOT NULL,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `nivel` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1' COMMENT '0=adm; 1=usuario',
  `excluido` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `usuario_atividade`
--

CREATE TABLE `usuario_atividade` (
  `uid` int(5) NOT NULL COMMENT 'fk usuario(uid)',
  `id_questao` int(5) NOT NULL COMMENT 'fk curso_questao(id)',
  `resp` enum('1','2','3','4') COLLATE utf8_bin NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `usuario_avaliacao`
--

CREATE TABLE `usuario_avaliacao` (
  `uid` int(5) NOT NULL COMMENT 'fk usuario(uid)',
  `id_curso` int(5) NOT NULL COMMENT 'fk curso(id)',
  `nota` tinyint(4) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentario` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `usuario_curso`
--

CREATE TABLE `usuario_curso` (
  `uid` int(5) NOT NULL COMMENT 'fk usuario(uid)',
  `id_curso` int(5) NOT NULL COMMENT 'fk curso(id)',
  `aprovado` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `usuario_video`
--

CREATE TABLE `usuario_video` (
  `uid` int(5) NOT NULL COMMENT 'fk usuario(uid)',
  `id_video` int(5) NOT NULL COMMENT 'fk curso_video(id)',
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curso_atividade`
--
ALTER TABLE `curso_atividade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unidade` (`id_unidade`) USING BTREE;

--
-- Indexes for table `curso_material`
--
ALTER TABLE `curso_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_curso` (`id_curso`);

--
-- Indexes for table `curso_questao`
--
ALTER TABLE `curso_questao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_atividade` (`id_atividade`) USING BTREE;

--
-- Indexes for table `curso_unidade`
--
ALTER TABLE `curso_unidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_curso` (`id_curso`);

--
-- Indexes for table `curso_video`
--
ALTER TABLE `curso_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unidade` (`id_unidade`);

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_uid` (`uid`),
  ADD KEY `fk_curso` (`id_curso`);

--
-- Indexes for table `forum_msg`
--
ALTER TABLE `forum_msg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_forum` (`id_forum`),
  ADD KEY `fk_usuario` (`uid`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`uid`,`uname`);

--
-- Indexes for table `usuario_atividade`
--
ALTER TABLE `usuario_atividade`
  ADD PRIMARY KEY (`uid`,`id_questao`),
  ADD KEY `fk_usuario` (`uid`),
  ADD KEY `fk_questao` (`id_questao`) USING BTREE;

--
-- Indexes for table `usuario_avaliacao`
--
ALTER TABLE `usuario_avaliacao`
  ADD PRIMARY KEY (`uid`,`id_curso`),
  ADD KEY `fk_usuario` (`uid`),
  ADD KEY `fk_curso` (`id_curso`);

--
-- Indexes for table `usuario_curso`
--
ALTER TABLE `usuario_curso`
  ADD PRIMARY KEY (`uid`,`id_curso`),
  ADD KEY `fk_uid` (`uid`),
  ADD KEY `fk_curso` (`id_curso`);

--
-- Indexes for table `usuario_video`
--
ALTER TABLE `usuario_video`
  ADD PRIMARY KEY (`uid`,`id_video`),
  ADD KEY `fk_usuario` (`uid`),
  ADD KEY `fk_video` (`id_video`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso_atividade`
--
ALTER TABLE `curso_atividade`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso_material`
--
ALTER TABLE `curso_material`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso_questao`
--
ALTER TABLE `curso_questao`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso_unidade`
--
ALTER TABLE `curso_unidade`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso_video`
--
ALTER TABLE `curso_video`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forum_msg`
--
ALTER TABLE `forum_msg`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `uid` int(5) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `curso_atividade`
--
ALTER TABLE `curso_atividade`
  ADD CONSTRAINT `ca_unidade` FOREIGN KEY (`id_unidade`) REFERENCES `curso_unidade` (`id`);

--
-- Constraints for table `curso_material`
--
ALTER TABLE `curso_material`
  ADD CONSTRAINT `cm_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id`);

--
-- Constraints for table `curso_questao`
--
ALTER TABLE `curso_questao`
  ADD CONSTRAINT `cq_atividade` FOREIGN KEY (`id_atividade`) REFERENCES `curso_atividade` (`id`);

--
-- Constraints for table `curso_unidade`
--
ALTER TABLE `curso_unidade`
  ADD CONSTRAINT `cu_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id`);

--
-- Constraints for table `curso_video`
--
ALTER TABLE `curso_video`
  ADD CONSTRAINT `cv_unidade` FOREIGN KEY (`id_unidade`) REFERENCES `curso_unidade` (`id`);

--
-- Constraints for table `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `f_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `f_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`);

--
-- Constraints for table `forum_msg`
--
ALTER TABLE `forum_msg`
  ADD CONSTRAINT `fm_forum` FOREIGN KEY (`id_forum`) REFERENCES `forum` (`id`),
  ADD CONSTRAINT `fm_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`);

--
-- Constraints for table `usuario_atividade`
--
ALTER TABLE `usuario_atividade`
  ADD CONSTRAINT `ua_questao` FOREIGN KEY (`id_questao`) REFERENCES `curso_questao` (`id`),
  ADD CONSTRAINT `ua_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`);

--
-- Constraints for table `usuario_avaliacao`
--
ALTER TABLE `usuario_avaliacao`
  ADD CONSTRAINT `uav_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `uav_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`);

--
-- Constraints for table `usuario_curso`
--
ALTER TABLE `usuario_curso`
  ADD CONSTRAINT `uc_curso` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `uc_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`);

--
-- Constraints for table `usuario_video`
--
ALTER TABLE `usuario_video`
  ADD CONSTRAINT `uv_usuario` FOREIGN KEY (`uid`) REFERENCES `usuario` (`uid`),
  ADD CONSTRAINT `uv_video` FOREIGN KEY (`id_video`) REFERENCES `curso_video` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
