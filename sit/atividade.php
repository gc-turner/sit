<?php

if($usuario_logado->nivel == 0)
   erro("Você não tem autorização para acessar esta página.");

if(!cursoUsuario($curso, $usuario_logado->uid))
   erro("Você não está inscrito neste curso.", "cursos");

$sql = "SELECT Cq.id, Cq.questao, Cq.alt1, Cq.alt2, Cq.alt3, Cq.alt4 FROM ((curso_questao AS Cq INNER JOIN curso_atividade AS Ca ON Cq.id_atividade = Ca.id) INNER JOIN curso_unidade AS Cu ON Ca.id_unidade = Cu.id) INNER JOIN curso AS C ON Cu.id_curso = C.id WHERE Ca.id = '$atividade' AND Cu.id = '$unidade' AND C.id = '$curso' ORDER BY Cq.id";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);

?>

<div class="row">
   <div class="col-xs-12 col-md-8 col-md-offset-2">
      <form role="form" class="panel panel-default" method="post" action="" id="frmAtividade" name="frmAtividade">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
         <input type="hidden" id="atividade" name="atividade" value="<?= $atividade; ?>" />
         <div class="panel-heading">
            <h3 class="panel-header"><?= get_atividadeUnidade($unidade, $atividade); ?></h3>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-xs-12">
                  <div class="nav-tabs">
                     <ul class="nav nav-tabs">
                        <?php
                        $active = " class='active'";
                        $aExp = " aria-expanded='true'";
                        for($i = 1; $i <= $result->rowCount(); $i++){
                           echo "<li$active><a data-toggle='tab' href='#qst$i'$aExp>Questão " . ($i < 10 ? ("0" . $i) : $i) . "</a></li>";
                           $active = "";
                           $aExp = "";
                        } ?>
                     </ul>
                     <div class="tab-content">
                        <?php
                        $i = 1;
                        $active = " active";
                        while($linha = $result->fetch()){
                           echo "<div class='tab-pane$active' id='qst$i'>
                              <div class='row'>
                                 <div class='col-xs-12 qsts'>
                                    <div class='form-group col-xs-12'>
                                       <p class='secao-corpo'>{$linha['questao']}</p>
                                    </div>
                                    <div class='form-group col-xs-12 alt'>
                                       <div class='col-xs-2 col-md-1 text-center'>
                                          <input type='radio' class='simple' id='{$linha['id']}_alt1' name='qst{$i}alt' />
                                       </div>
                                       <div class='col-xs-10'>
                                          <p class='secao-corpo'>{$linha['alt1']}</p>
                                       </div>
                                    </div>
                                    <div class='form-group col-xs-12 alt'>
                                       <div class='col-xs-2 col-md-1 text-center'>
                                          <input type='radio' class='simple' id='{$linha['id']}_alt2' name='qst{$i}alt' />
                                       </div>
                                       <div class='col-xs-10'>
                                          <p class='secao-corpo'>{$linha['alt2']}</p>
                                       </div>
                                    </div>
                                    <div class='form-group col-xs-12 alt'>
                                       <div class='col-xs-2 col-md-1 text-center'>
                                          <input type='radio' class='simple' id='{$linha['id']}_alt3' name='qst{$i}alt' />
                                       </div>
                                       <div class='col-xs-10'>
                                          <p class='secao-corpo'>{$linha['alt3']}</p>
                                       </div>
                                    </div>
                                    <div class='form-group col-xs-12 alt'>
                                       <div class='col-xs-2 col-md-1 text-center'>
                                          <input type='radio' class='simple' id='{$linha['id']}_alt4' name='qst{$i}alt' />
                                       </div>
                                       <div class='col-xs-10'>
                                          <p class='secao-corpo'>{$linha['alt4']}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>";
                           $active = "";
                           $i++;
                        } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" id="btnVoltar" onclick="$('#frmCurso').submit();">Voltar</button>
            <button type="button" class="btn" id="btnGravar"></button>            
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $curso; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= get_nomeCurso($curso) . " - " . get_unidadeCurso($curso, $unidade); ?>");
      $(".alt:odd").css("background-color", "#cccccc");
      chkAtividade(<?= "'$atividade'"; ?>);
   });
</script>