<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$usuarios = array();
$sql = "SELECT uid, nome FROM usuario WHERE excluido = '0' AND nivel = '1' ORDER BY nome";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);

?>

<div class="row">
   <div class="col-xs-12 col-md-4 col-md-offset-4">
      <form role="form" class="panel panel-default" method="post" action="" id="frmUsr" name="frmUsr">
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="usr">Usuário <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <select class="form-control" id="usr" name="usr">
                     <option value="" selected>Selecione</option>
                     <?php
                     while($linha = $result->fetch()){
                        echo "<option value='{$linha['uid']}'>{$linha['nome']}</option>";
                     }
                     ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" onclick="direciona('inicio');">Voltar</button>
            <button type="button" class="btn btn-primary" onclick="relUsuario();">Pesquisar</button>
         </div>
      </form>
   </div>
</div>

<div class="row hide">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <div class="panel panel-info">
         <div class="panel-heading">
            <h3 class="panel-header">Resultado</h3>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-xs-12" id="resultado"></div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Relatório");
   });
</script>