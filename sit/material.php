<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$nomeCurso = get_nomeCurso($curso);

?>

<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="material_ins.php" id="frmMaterial" name="frmMaterial" enctype="multipart/form-data">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="nome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="nome" name="nome" value="" />
               </div>
               <div class="form-group col-xs-12">
                  <label for="arquivo">Arquivo <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="file" id="arquivo" name="arquivo" value="" />
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" onclick="$('#frmCurso').submit();">Voltar</button>
            <button type="button" class="btn btn-success" onclick="validarMaterial();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $curso; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= $nomeCurso; ?> - Material");
   });
</script>