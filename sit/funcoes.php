<?php
function erro($msg, $pagina = "inicio"){
   if($pagina == "inicio")
      $botao = "Página inicial";
   else
      $botao = "Prosseguir";
   
   echo "
      <div class='row'>
         <div class='col-xs-12 col-md-6 col-md-offset-3'>
            <div class='panel panel-danger'>
               <div class='panel-heading'>
                  <p class='panel-title'><em class='glyphicon glyphicon-ban-circle'></em> Não disponível!</p>
               </div>
               <div class='panel-body'>
                  <div class='row'>
                     <div class='col-xs-12'>
                        <p>$msg</p>
                     </div>
                  </div>
               </div>
               <div class='panel-footer'>
                  <button type='button' class='btn btn-primary' onclick='direciona(\"$pagina\");'>$botao</button>
               </div>
            </div>
         </div>
      </div>
   ";
   exit;
}

function cursoUsuario($curso, $usuario){
   GLOBAL $bd;
   
   $sql = "SELECT COUNT(*) FROM usuario_curso WHERE uid = '$usuario' AND id_curso = '$curso'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados.");
   $linha = $result->fetch();
   
   if(intval($linha[0]) == 0)
      return false;
   return true;
}

function gravaUsrVideo($usuario, $video, $curso){
   GLOBAL $bd;
   
   $data = date("Y-m-d H:i:s", time());
   $sql = "INSERT IGNORE INTO usuario_video (uid, id_video, data) VALUES ('$usuario', '$video', '$data') ON DUPLICATE KEY UPDATE data = '$data'";
   $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
   verificaUsrAprov($usuario, $curso);
}

function verificaUsrAprov($usuario, $curso){
   GLOBAL $bd;
   
   //quantidade videos do curso
   $qtVideos = get_qtdVideos($curso);
   //quantidade videos do curso acessados pelo usuario
   $qtUsrVideos = get_qtdVideos($curso, $usuario);
   //nota do usuario
   $usrNota = get_notaUsr($curso, $usuario);
   
   $run = false;
   if($qtVideos == $qtUsrVideos && ($usrNota >= 70)){
      if(!usrAprov($usuario, $curso))
         $run = true;
      $sql = "UPDATE usuario_curso SET aprovado = '1' WHERE uid = '$usuario' AND id_curso = '$curso'";
   } else {
      if(usrAprov($usuario, $curso))
         $run = true;
      $sql = "UPDATE usuario_curso SET aprovado = '0' WHERE uid = '$usuario' AND id_curso = '$curso'";
   }
   
   if($run)
      $bd->exec($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
   
}

function get_notaUsr($curso, $usuario){
   GLOBAL $bd;
   
   //quantidade questoes do curso
   $qtQuestoes = get_qtdQuestoes($curso);
   //quantidade questoes do curso respondidas corretamente pelo usuario
   $qtUsrQuestoes = get_qtdQuestoes($curso, $usuario);
   
   if($qtQuestoes > 0)
      return round(floatval(($qtUsrQuestoes * 100) / $qtQuestoes));
   else
      return 0;
}

function get_qtdVideos($curso, $usuario = ""){
   GLOBAL $bd;
   
   if($usuario == "")
      $sql = "SELECT COUNT(CV.id) FROM curso_video AS CV INNER JOIN curso_unidade AS CU ON CV.id_unidade = CU.id WHERE CU.id_curso = '$curso'";
   else
      $sql = "SELECT COUNT(UV.id_video) FROM (usuario_video AS UV INNER JOIN curso_video AS CV ON UV.id_video = CV.id) INNER JOIN curso_unidade AS CU ON CV.id_unidade = CU.id WHERE UV.uid = '$usuario' AND CU.id_curso = '$curso'";
   
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   
   if($linha = $result->fetch())
      return intval($linha[0]);
   return 0;
}

function get_qtdQuestoes($curso, $usuario = ""){
   GLOBAL $bd;
   
   if($usuario == "")
      $sql = "SELECT COUNT(CQ.id) FROM (curso_questao AS CQ INNER JOIN curso_atividade AS CA ON CQ.id_atividade = CA.id) INNER JOIN curso_unidade AS CU ON CA.id_unidade = CU.id WHERE CU.id_curso = '$curso'";
   else
      $sql = "SELECT COUNT(UA.id_questao) FROM ((usuario_atividade AS UA INNER JOIN curso_questao AS CQ ON UA.id_questao = CQ.id) INNER JOIN curso_atividade AS CA ON CQ.id_atividade = CA.id) INNER JOIN curso_unidade AS CU ON CA.id_unidade = CU.id WHERE UA.uid = '$usuario' AND CU.id_curso = '$curso' AND UA.resp = CQ.resp";
   
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   
   if($linha = $result->fetch())
      return intval($linha[0]);
   return 0;
}

function usrAprov($usuario, $curso){
   GLOBAL $bd;
   
   $sql = "SELECT aprovado FROM usuario_curso WHERE uid = '$usuario' AND id_curso = '$curso'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   if($linha = $result->fetch())
      return $linha[0] == "1" ? true : false;
   return false;
}

function get_nomeUsuario($uid){
   GLOBAL $bd;
   
   $sql = "SELECT nome FROM usuario WHERE uid = '$uid'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados.");
   
   if($linha = $result->fetch())
      return $linha[0];
   return "Não definido";
}

function get_nomeCurso($curso){
   GLOBAL $bd;
   
   $sql = "SELECT nome FROM curso WHERE id = '$curso'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados.");
   
   if($linha = $result->fetch())
      return $linha[0];
   return "Não definido";
}

function get_unidadeCurso($curso, $unidade){
   GLOBAL $bd;
   
   $sql = "SELECT nome FROM curso_unidade WHERE id = '$unidade' AND id_curso = '$curso'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados.");
   
   if($linha = $result->fetch())
      return $linha[0];
   return "Não definido";
}

function get_atividadeUnidade($unidade, $atividade){
   GLOBAL $bd;
   
   $sql = "SELECT nome FROM curso_atividade WHERE id = '$atividade' AND id_unidade = '$unidade'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados.");
   
   if($linha = $result->fetch())
      return $linha[0];
   return "Não definido";
}

function get_dataExtenso($data){   
   $meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
   $arrDt = explode("/", $data);
   
   $dia = $arrDt[0];
   $mes = $meses[$arrDt[1] - 1];
   $ano = $arrDt[2];
   
   return "$dia de $mes de $ano.";
}
?>