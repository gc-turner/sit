<?php

if($usuario_logado->nivel == 0)
   erro("Você não tem autorização para acessar esta página.");

if(cursoUsuario($Cid, $usuario_logado->uid)) { ?>
   <p class="secao-corpo">
      <em class="glyphicon glyphicon-comment"></em> <a onclick="$('#frmForum').submit();">Fórum</a>
   </p>
   <p class="secao-corpo">
      <em class="glyphicon glyphicon-signal"></em> <a onclick="$('#frmAval').submit();">Avalie o curso</a>
   </p>
   <?php if(usrAprov($usuario_logado->uid, $Cid)) {
      $cert = base64_encode($Cid . "_" . time());
   ?>
   <p class="secao-corpo">
      <em class="glyphicon glyphicon-education"></em>&nbsp;
      <a onclick="window.open('certificado.php?id=<?= $cert; ?>');">
         Certificado
      </a>
   </p>
   <?php } ?>
</div>
<?php 
   $sql = "SELECT id, nome FROM curso_material WHERE id_curso = '$Cid' ORDER BY id";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   if($linha = $result->fetch()){
      echo "<div class='form-group col-xs-12'>";
      while($linha){
         $file = base64_encode($Cid . "_" .$linha['id'] . "_" . time());
         echo "<p class='secao-corpo'>
               <em class='glyphicon glyphicon-file'></em>&nbsp;
               <a onclick='window.open(\"download.php?id=$file\");'>
               {$linha['nome']}
               </a>
            </p>";
         $linha = $result->fetch();
      }
      echo "</div>";
   }
   $sql = "SELECT id, nome FROM curso_unidade WHERE id_curso = '$Cid' ORDER BY id";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   if($linha = $result->fetch()){
      echo "<div class='form-group col-xs-12'>";
      while($linha){
         $hr = "";
         echo "<p class='secao-titulo'><em class='glyphicon glyphicon-asterisk'></em>&nbsp;{$linha['nome']}</p>";
         //Videos
         $sql = "SELECT id, nome FROM curso_video WHERE id_unidade = '{$linha['id']}' ORDER BY id";
         $rItem = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
         if($lItem = $rItem->fetch()){
            $hr = "<hr />";
            while($lItem){
               $vid = base64_encode($lItem['id'] . "_" . time());
               echo "<p class='secao-corpo'><em class='glyphicon glyphicon-film'></em>&nbsp;<a data-toggle='tooltip' title='Exibir vídeo' onclick='exibirVideo(\"$vid\");'>{$lItem['nome']}</a></p>";
               $lItem = $rItem->fetch();
            }
         }
         //Atividades
         $sql = "SELECT id, nome FROM curso_atividade WHERE id_unidade = '{$linha['id']}' AND id IN (SELECT id_atividade FROM curso_questao) ORDER BY id";
         $rItem = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
         if($lItem = $rItem->fetch()){
            echo $hr;
            while($lItem){
               echo "<p class='secao-corpo'><em class='glyphicon glyphicon-list-alt'></em>&nbsp;<a data-toggle='tooltip' title='Abrir atividade' onclick='abrirAtividade(\"{$linha['id']}\", \"{$lItem['id']}\");'>{$lItem['nome']}</a></p>";
               $lItem = $rItem->fetch();
            }
         }
         $linha = $result->fetch();
      }
      echo "</div>";
   }
?>
<form role="form" id="frmAval" name="frmAval" method="post" action="./">
   <input type="hidden" id="page" name="page" value="avaliacao" />
   <input type="hidden" id="curso" name="curso" value="<?= $Cid; ?>" />
</form>

<form role="form" id="frmAtividade" name="frmAtividade" method="post" action="./">
   <input type="hidden" id="page" name="page" value="atividade" />
   <input type="hidden" id="curso" name="curso" value="<?= $Cid; ?>" />
   <input type="hidden" id="unidade" name="unidade" value="" />
   <input type="hidden" id="atividade" name="atividade" value="" />
</form>
<?php } else { ?>
</div>
<div class="form-group col-xs-12">
   <p class="secao-corpo">
      Para ter acesso ao conteúdo completo deste curso, é necessário se inscrever.
   </p>   
</div>
<div class="form-group col-xs-12 text-center">
   <button type="button" class="btn btn-primary" onclick="cursoInsc(<?= "'$Cid', '$usuario_logado->uid'"; ?>);">Inscrever</button>
</div>
<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $Cid; ?>" />
</form>
<?php } ?>