<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

?>

<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="curso_ins.php" id="frmCurso" name="frmCurso">
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="nome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="nome" name="nome" value="" />
               </div>
               <div class="form-group col-xs-12">
                  <label for="instrutor">Instrutor <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="instrutor" name="instrutor" value="" />
               </div>
               <div class="form-group col-xs-12 col-md-6">
                  <label for="categoria">Categoria <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="categoria" name="categoria" value="" />
               </div>
               <div class="form-group col-xs-12">
                  <label for="pchave">Palavras-Chave</label>
                  <textarea class="form-control" rows="4" maxlength="200" id="pchave" name="pchave" value=""></textarea>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" onclick="direciona('cursos');">Voltar</button>
            <button type="button" class="btn btn-success" onclick="validarCurso();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Cadastro de Cursos");
   });
</script>