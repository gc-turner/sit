<?php
if($usuario_logado->nivel == 1 && !cursoUsuario($curso, $usuario_logado->uid))
   erro("Você não está inscrito neste curso.", "cursos");

$sql = "SELECT uid, topico, msg, data FROM forum WHERE id = '$topico'";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
if(!$linha = $result->fetch())
   erro("Tópico não encontrado.", "cursos");
?>

<div id="posts" class="row">
   <div class="col-xs-12 col-md-8 col-md-offset-2">
      <div class="panel panel-info">
         <div class="panel-heading">
            <h3 class="panel-header"><?= "{$linha['topico']} <small class='pull-right'>" . get_nomeUsuario($linha['uid']) . " [" . date("d/m/Y H:i:s", strtotime($linha['data'])) ."]</small>"; ?></h3>
         </div>
         <div class="panel-body">
            <?= "<p class='secao-corpo'>". nl2br($linha['msg']) . "</p>"; ?>
         </div>
      </div>
      <div id="lstMsg"></div>
      <div class="form-group col-xs-12">
         <button type="button" class="btn btn-warning" onclick="$('#frmForum').submit();">Voltar</button>
         <button type="button" class="btn btn-primary" id="btnAdd">Responder</button>         
      </div>
   </div>
</div>

<div id="mensagem" class="row hide">
   <div class="col-xs-12 col-md-8 col-md-offset-2">
      <form role="form" class="panel panel-default" method="post" action="" id="frmMsg" name="frmMsg">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <input type="hidden" id="topico" name="topico" value="<?= $topico; ?>" />
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="msg">Mensagem <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <textarea class="form-control" rows="4" id="msg" name="msg" value=""></textarea>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" id="btnCancelar">Cancelar</button>
            <button type="button" class="btn btn-success" onclick="validarMsg();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmForum" name="frmForum" method="post" action="./">
   <input type="hidden" id="page" name="page" value="forum" />
   <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= get_nomeCurso($curso) . " - Fórum"; ?>");
      
      $("#btnAdd").unbind("click");
      $("#btnCancelar").unbind("click");
      
      $("#btnAdd").click(function(e){
         $("div#posts").toggleClass("hide");
         $("div#mensagem").toggleClass("hide");
         $("#frmForum #msg").val("");
      });
      
      $("#btnCancelar").click(function(e){
         $("div#posts").toggleClass("hide");
         $("div#mensagem").toggleClass("hide");
      });
      
      lstMensagens("<?= $topico; ?>");
   });
</script>