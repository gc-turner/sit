<?php
require_once("config.php");

$file = base64_decode($id);
$file = explode("_", $file);
$curso = $file[0];
$material = $file[1];

if($usuario_logado->nivel == 1 && !cursoUsuario($curso, $usuario_logado->uid))
   die("É necessário se inscrever no curso para ter acesso aos materiais.");

$sql = "SELECT * FROM curso_material WHERE id = '$material'";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
$linha = $result->fetch();

header("Content-disposition: attachment; filename=\"" . str_replace(" ", "_", $linha['nome']) . "." . $linha['tipo'] . "\"");
header("Content-type: application/octetstream");
header("Pragma: ");
header("Cache-Control: cache");
header("Expires: 0");
echo "{$linha['arquivo']}";

?>