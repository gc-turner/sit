<?php
require_once("config.php");

$crs = base64_decode($id);
$crs = explode("_", $crs);
$curso = $crs[0];

if($usuario_logado->nivel == 0 || !cursoUsuario($curso, $usuario_logado->uid) || !usrAprov($usuario_logado->uid, $curso))
   die("Você não tem autorização para acessar esta página.");

require_once("lib/TCPDF/tcpdf.php");
// Baseado em: Example 051 : image as a page background
// https://tcpdf.org/examples/example_051/
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = 'img/cert.png';
        $this->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

// create new PDF document
$pdf = new MYPDF('l', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Thiago H. Pereira');
$pdf->SetTitle('SIT - Certificado de conclusão');
$pdf->SetSubject('Certificado de conclusão');
$pdf->SetKeywords('SIT, Sistema Interno de Treinamentos, Certificado');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('50', '55', '50');
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set font
$pdf->SetFont('times', '', 24);

// add a page
$pdf->AddPage();

// Print a text
$html = "<div><p style=\"text-align:justify;font-size:30pt\">Certificamos que <strong>{$usuario_logado->nome}</strong> participou e obteve aprovação no curso: <u>" . get_nomeCurso($curso) . "</u>.</p></div><div><p style=\"text-align:right\">" . get_dataExtenso(date("d/n/Y", time())) . "</p></div>";
$pdf->writeHTML($html, true, false, true, false, '');

//Close and output PDF document
$pdf->Output('Certificado_' . $usuario_logado->nome . '.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+