<?php
require_once("cabecalho.php");

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$arquivo = isset($_FILES["vFile"]) ? $_FILES["vFile"] : FALSE;

if($arquivo['tmp_name'] != "" && ($arquivo['size'] > 0 && $arquivo['size'] <= (1024 * 1024 * 40))){ //40MB
   $sql = "INSERT INTO curso_video (id_unidade, nome, externo) VALUES ('$unidade', '$vNome', '0')";
   $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
   
   $msg = "Vídeo inserido.";
   
   $nome = base64_encode($bd->lastInsertId() . $vNome . $curso . $unidade);
   $arq = explode(".", $arquivo['name']);
   $arq_tipo = $arq[count($arq) - 1];
   
   $video = move_uploaded_file($arquivo['tmp_name'], "videos/$nome.mp4");
   
   if($arq_tipo != "mp4" || !$video){
      $sql = "DELETE FROM curso_video WHERE id = '" . $bd->lastInsertId() . "'";
      $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
      $msg = "Erro ao fazer o upload do vídeo.";
   }
}
else {
   $msg = "Erro:\\nArquivo inválido!";
}

?>
   </head>
   <body>
      <form role="form" id="frmVideo" name="frmVideo" method="post" action="./">
         <input type="hidden" id="page" name="page" value="unidade" />
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
      </form>
      
      <script>         
         alert("<?= $msg; ?>");
         $("#frmVideo").submit();
      </script>
      <?php
      $bd = null;
      ?>
   </body>
</html>
