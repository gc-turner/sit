<?php
require_once("config.php");

switch($funcao){
   case "consultaUsuario":
      $sql = "SELECT COUNT(*) FROM usuario WHERE uname = '$uname'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      $linha = $result->fetch();
      if(intval($linha[0]) ==  0)
         echo json_encode(array("retorno" => "OK"));
      else
         echo json_encode(array("retorno" => "Usuário duplicado", "msg" => "Já existe um usuário com este login."));
      break;
   case "excluirUsuario":
      $sql = "UPDATE usuario SET excluido = '1' WHERE uid = '$uid'";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível excluir o usuário.")));
      echo json_encode(array("retorno" => "OK"));
      break;
   case "alteraSenha":
      $sql = "SELECT upass FROM usuario WHERE uid = '$uid'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()){
         if(password_verify($opass, trim($linha['upass']))){
            $hpass = password_hash($npass, PASSWORD_DEFAULT);
            $hpass = trim($hpass);
            
            $sql = "UPDATE usuario SET upass = '$hpass' WHERE uid = '$uid'";
            $bd->exec($sql) or die(json_ecode(array("retorno" => "Erro", "msg" => "Não foi possível alterar a senha.")));
            echo json_encode(array("retorno" => "OK"));
         } else
            echo json_encode(array("retorno" => "Erro", "msg" => "Senha atual não confere."));
      } else
         echo json_encode(array("retorno" => "Erro", "msg" => "Usuário não encontrado."));
      break;
   case "inscUsr":
      $sql = "INSERT IGNORE INTO usuario_curso (uid, id_curso) VALUES ('$uid', '$curso')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível realizar a inscrição.")));
      echo json_encode(array("retorno" => "OK"));
      break;
   case "insUnidade":
      $sql = "INSERT INTO curso_unidade (id_curso, nome) VALUES ('$curso', '$nome')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível inserir a unidade.")));
      echo json_encode(array("retorno" => "OK", "msg" => $bd->lastInsertId()));
      break;
   case "getVideos":
      $sql = "SELECT id, nome FROM curso_video WHERE id_unidade = '$unidade' ORDER BY id";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      $videos = array();   
      if($linha = $result->fetch()){
         while($linha){
            $videos[] = array("id" => base64_encode($linha['id'] . "_" . time()), "nome" => $linha['nome']);
            $linha = $result->fetch();
         }
      } else {
         $videos[] = "Nenhum vídeo cadastrado.";
      }
      echo json_encode(array("retorno" => "OK", "msg" => $videos));
      break;
   case "insVideo":
      $sql = "INSERT INTO curso_video (id_unidade, nome, externo, link, plataforma) VALUES ('$unidade', '$nome', '1', '$link', '$plataforma')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível cadastrar o vídeo.")));
      echo json_encode(array("retorno" => "OK"));
      break;
   case "getAtividades":
      $sql = "SELECT id, nome FROM curso_atividade WHERE id_unidade = '$unidade' ORDER BY id";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      $atividades = array();   
      if($linha = $result->fetch()){
         while($linha){
            $atividades[] = array("id" => $linha['id'], "nome" => $linha['nome']);
            $linha = $result->fetch();
         }
      } else {
         $atividades[] = "Nenhuma atividade cadastrada.";
      }
      echo json_encode(array("retorno" => "OK", "msg" => $atividades));
      break;
   case "insAtividade":
      $sql = "INSERT INTO curso_atividade (id_unidade, nome) VALUES ('$unidade', '$nome')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível inserir a unidade.")));
      echo json_encode(array("retorno" => "OK", "msg" => $bd->lastInsertId()));
      break;
   case "chkAtividade":
      $sql = "SELECT Cq.id, Cq.resp AS cresp, Ua.resp AS uresp FROM usuario_atividade AS Ua INNER JOIN curso_questao AS Cq ON Ua.id_questao = Cq.id WHERE Cq.id_atividade = '$atividade' AND Ua.uid = '{$usuario_logado->uid}'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()){
         $dados = array();
         while($linha){
            $resp = $linha['cresp'] == $linha['uresp'] ? 1 : 0;
            $dados[] = array("id" => $linha['id'], "alt" => $linha['uresp'], "resp" => $resp);
            $linha = $result->fetch();
         }
         echo json_encode(array("retorno" => "OK", "msg" => $dados));
      } else
         echo json_encode(array("retorno" => ""));
      break;
   case "gravaAtividade":
      $dados = json_decode($dados);
      $data = date("Y-m-d H:i:s", time());
      for($i = 0; $i < count($dados); $i++){
         $qst = explode("_", $dados[$i]);
         $qID = $qst[0];
         $resp = str_replace("alt", "", $qst[1]);
         $sql = "INSERT IGNORE INTO usuario_atividade (uid, id_questao, resp, data) VALUES ('{$usuario_logado->uid}', '$qID', '$resp', '$data') ON DUPLICATE KEY UPDATE resp = '$resp', data = '$data'";
         $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível acessar a base de dados.")));
      }
      verificaUsrAprov($usuario_logado->uid, $curso);
      echo json_encode(array("retorno" => "OK"));
      break;
   case "getQuestoes":
      $sql = "SELECT CQ.id FROM curso_questao AS CQ INNER JOIN curso_atividade AS CA ON CA.id = CQ.id_atividade WHERE CA.id = '$atividade' AND CA.id_unidade = '$unidade' ORDER BY CQ.id";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      $questoes = array();   
      if($linha = $result->fetch()){
         while($linha){
            $questoes[] = $linha['id'];
            $linha = $result->fetch();
         }
      } else {
         $questoes = "Nenhuma questão cadastrada.";
      }
      echo json_encode(array("retorno" => "OK", "msg" => $questoes));
      break;
   case "insQuestao":
      if($qID == "") {
         $sql = "INSERT INTO curso_questao (id_atividade, questao, alt1, alt2, alt3, alt4, resp) VALUES ('$atividade', '$questao', '$alt1', '$alt2', '$alt3', '$alt4', '$resp')";
         $msg = "Questão cadastrada.";
      } else {
         $sql = "UPDATE curso_questao SET questao = '$questao', alt1 = '$alt1', alt2 = '$alt2', alt3 = '$alt3', alt4 = '$alt4', resp = '$resp' WHERE id = '$qID' AND id_atividade = '$atividade'";
         $msg = "Questão atualizada.";
      }
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível acessar a base de dados.")));
      echo json_encode(array("retorno" => "OK", "msg" => $msg));
      break;
   case "getQuestao":
      $sql = "SELECT questao, alt1, alt2, alt3, alt4, resp FROM curso_questao WHERE id = '$qID' AND id_atividade = '$atividade'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      $questao = array();   
      if($linha = $result->fetch()){
         $questao['questao'] = $linha['questao'];
         $questao['alt1'] = $linha['alt1'];
         $questao['alt2'] = $linha['alt2'];
         $questao['alt3'] = $linha['alt3'];
         $questao['alt4'] = $linha['alt4'];
         $questao['resp'] = $linha['resp'];
         
         echo json_encode(array("retorno" => "OK", "msg" => $questao));
      } else
         echo json_encode(array("retorno" => "Erro", "msg" => "Questão não cadastrada."));
      break;
   case "chkAvaliacao":
      $sql = "SELECT nota, comentario FROM usuario_avaliacao WHERE id_curso = '$curso' AND uid = '{$usuario_logado->uid}'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()){
         $comentario = isset($linha['comentario']) ? $linha['comentario'] : "";
         $dados = array("nota" => $linha['nota'], "comentario" => $comentario);
         echo json_encode(array("retorno" => "OK", "msg" => $dados));
      } else
         echo json_encode(array("retorno" => ""));
      break;
   case "insAvaliacao":
      $data = date("Y-m-d H:i:s", time());
      $comentario = $comentario == "" ? "NULL" : "'$comentario'";
      $sql = "INSERT IGNORE INTO usuario_avaliacao (uid, id_curso, nota, comentario, data) VALUES ('{$usuario_logado->uid}', '$curso', '$nota', $comentario, '$data') ON DUPLICATE KEY UPDATE nota = '$nota', comentario = $comentario, data = '$data'";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível cadastrar sua avaliação.")));
      echo json_encode(array("retorno" => "OK"));
      break;
   case "getForuns":
      $sql = "SELECT F.id, F.uid, F.topico, F.data, FM.ultMsg FROM forum AS F LEFT OUTER JOIN (SELECT id_forum, MAX(data) AS ultMsg FROM forum_msg GROUP BY id_forum) AS FM ON FM.id_forum = F.id WHERE F.id_curso = '$curso' ORDER BY F.id ASC";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()){
         $dados = array();
         while($linha){
            $key = isset($linha['ultMsg']) ? strtotime($linha['ultMsg']) : strtotime($linha['data']);
            $data = isset($linha['ultMsg']) ? date("d/m/Y H:i:s", strtotime($linha['ultMsg'])) : date("d/m/Y H:i:s", strtotime($linha['data']));
            $dados[$key] = array("forum" => $linha['id'], "autor" => get_nomeUsuario($linha['uid']), "topico" => $linha['topico'], "data" => $data);
            $linha = $result->fetch();
         }
         krsort($dados);
         $foruns = array();
         foreach($dados as $value)
            $foruns[] = $value;
         echo json_encode(array("retorno" => "OK", "msg" => $foruns));
      } else
         echo json_encode(array("retorno" => "OK", "msg" => ""));
      break;
   case "insTopico":
      $data = date("Y-m-d H:i:s", time());
      $msg = $msg == "" ? "NULL" : "'$msg'";
      $sql = "INSERT INTO forum (uid, id_curso, topico, msg, data) VALUES ('{$usuario_logado->uid}', '$curso', '$topico', $msg, '$data')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível cadastrar o tópico.")));
      echo json_encode(array("retorno" => "OK", "msg" => $bd->lastInsertId()));
      break;
   case "getMensagens":
      $sql = "SELECT uid, msg, data FROM forum_msg WHERE id_forum = '$topico' ORDER BY id";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()){
         $dados = array();
         while($linha){
            $dados[] = array("autor" => get_nomeUsuario($linha['uid']), "msg" => nl2br($linha['msg']), "data" => date("d/m/Y H:i:s", strtotime($linha['data'])));
            $linha = $result->fetch();
         }
         echo json_encode(array("retorno" => "OK", "msg" => $dados));
     } else
         echo json_encode(array("retorno" => "OK", "msg" => "")); 
      break;
   case "insMensagem":
      $data = date("Y-m-d H:i:s", time());
      $msg = $msg == "" ? "NULL" : "'$msg'";
      $sql = "INSERT INTO forum_msg (uid, id_forum, msg, data) VALUES ('{$usuario_logado->uid}', '$topico', $msg, '$data')";
      $bd->exec($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível cadastrar o tópico.")));
      echo json_encode(array("retorno" => "OK"));
      break;
   case "relatorio":
      $sql = "SELECT C.id, C.nome, UC.aprovado FROM usuario_curso AS UC INNER JOIN curso AS C ON UC.id_curso = C.id WHERE UC.uid = '$uid'";
      $result = $bd->query($sql) or die(json_encode(array("retorno" => "Erro", "msg" => "Não foi possível consultar a base de dados.")));
      if($linha = $result->fetch()) {
         $dados = array();
         while($linha){
            //quantidade videos do curso
            $qtVideos = get_qtdVideos($linha['id']);
            //quantidade videos do curso acessados pelo usuario
            $qtUsrVideos = get_qtdVideos($linha['id'], $uid);
            //percentual videos assistidos
            $percVideos = $qtVideos == 0 ? 0 : round(floatval(($qtUsrVideos * 100) / $qtVideos));
            //nota do usuario
            $usrNota = get_notaUsr($linha['id'], $uid);
            
            $dados[] = array("curso" => $linha['nome'], "videos" => $percVideos, "nota" => $usrNota, "aprov" => intval($linha['aprovado']));
            
            $linha = $result->fetch();
         }
         echo json_encode(array("retorno" => "OK", "msg" => $dados));
      } else {
         echo json_encode(array("retorno" => "OK", "msg" => $result->rowCount()));
      }
      break;
}
?>