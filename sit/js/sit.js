"use strict";

$(document).ready(function(){
   $("[data-toggle='tooltip']").tooltip({
      html: true,
      placement: "auto top"
   });
});

function initControls($parent){
   $parent.find('[data-toggle="tooltip"]').tooltip();
}

function hideMenu(){
   if($(".side-menu").css("left") == "0px"){
      $(".side-menu").css("left", "-220px");
      $(".content").css("margin-left", "10px");
   }
   else{
      $(".side-menu").css("left", "0px");
      $(".content").css("margin-left", "210px");
   }
}

function direciona(pagina){
   $("#frmPage #page").val(pagina);
   $("#frmPage").submit();
}

// Usuários
function validarUsuario(){   
   if($("#frmUsr #nome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   if($("#frmUsr #uname").val() == ""){
      alert('O campo Usuário deve ser preenchido.');
      return;
   }
   if($("#frmUsr #nivel").val() == ""){
      alert('Selecione o Nível do Usuário.');
      return;
   }
   if($("#frmUsr #upass").val() == ""){
      alert('O campo Senha deve ser preenchido.');
      return;
   }
   if($("#frmUsr #upass").val().length < 8){
      alert('A senha deve ter no mínimo 8 caracteres.');
      return;
   }
   if(!$("#frmUsr #uname").attr("disabled")){
      $.ajax({
         url: "sitAJAX.php",
         data: "funcao=consultaUsuario&uname=" + $("#frmUsr #uname").val(),
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            $("#frmUsr").submit();   
         }
      });
   }
   $("#frmUsr").submit();
}

function alterarUsuario(uid){
   $("#frmUsrCh #uid").val(uid);
   $("#frmUsrCh").submit();
}

function excluirUsuario(uid){
   if(confirm("Deseja realmente excluir este usuário?")){
      $.ajax({
         url: "sitAJAX.php",
         data: "funcao=excluirUsuario&uid=" + uid,
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            alert("Usuário excluído.");
            direciona("usuario");
         }
      });
   }
   return;
}

function alterarSenha(){
   if($("#frmPass #opass").val() == "" || $("#frmPass #npass").val() == "" || $("#frmPass #cpass").val() == ""){
      alert("Preencha todos os campos.");
      return;
   }
   
   if($("#frmPass #npass").val().length < 8){
      alert('A nova senha deve ter no mínimo 8 caracteres.');
      return;
   }
   
   if($("#frmPass #npass").val() != $("#frmPass #cpass").val()){
      alert("O valor digitado no campo de confirmação é diferente ao da nova senha.");
      return;
   }
   
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=alteraSenha&uid=" + $("#frmPass #uid").val() + "&opass=" + $("#frmPass #opass").val() + "&npass=" + $("#frmPass #npass").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Senha alterada.\nRealize novamente o login.");
         window.location = "logout.php";
      }
   });
}
///////////////////////////////////////////////////////////////////
// Cursos
function abrirCurso(id){
   $("#frmCurso #id").val(id);
   $("#frmCurso").submit();
}

function validarCurso(){   
   if($("#frmCurso #nome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   if($("#frmCurso #instrutor").val() == ""){
      alert('O campo Instrutor deve ser preenchido.');
      return;
   }
   if($("#frmCurso #categoria").val() == ""){
      alert('O campo Categoria deve ser preenchido.');
      return;
   }
   $("#frmCurso").submit();
}

function editCurso(pagina, unidade){
   unidade = unidade || "";
   
   $("#frmAcao #page").val(pagina);
   $("#frmAcao #unidade").val(unidade);
   $("#frmAcao").submit();
}

function cursoInsc(curso, uID){
   if(curso != "" && uID != "")
      $.ajax({
         url: "sitAJAX.php",
         data: "funcao=inscUsr&curso=" + curso + "&uid=" + uID,
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            $("#frmCurso").submit();
         }
      });
}

function validarMaterial(){
   if($("#frmMaterial #nome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   if($("#frmMaterial #arquivo").val() == ""){
      alert('Selecione o arquivo desejado.');
      return;
   }
   $("#frmMaterial").submit();
}

function insUnidade(){
   if($("#frmUnidade #nome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insUnidade&curso=" + $("#frmUnidade #curso").val() + "&nome=" + $("#frmUnidade #nome").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Unidade cadastrada.");
         $("#frmUnidade #unidade").val(retorno.msg);
         $("#frmUnidade").submit();
      }
   });
}

function validarTipoVideo(){
   switch ($("input[type=radio][name=vTipo]:checked").val()){
      case "0":
         $(".divLocal").show();
         $(".divExterno").hide();
         break;
      case "1":
         $(".divLocal").hide();
         $(".divExterno").show();
         break;
   }
}

function lstVideos() {
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getVideos&unidade=" + $("#frmUnidade #unidade").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "";
         for(let video of retorno.msg)
            if(typeof video === "object")
               html += "<p class='secao-corpo'><em class='glyphicon glyphicon-film'></em>&nbsp;<a data-toggle='tooltip' title='Exibir vídeo' onclick='exibirVideo(\"" + video.id + "\");'>" + video.nome + "</a></p>";
            else
               html += "<p class='text-muted'>" + video + "</p>";
         $("#lstVideo").html(html);
         initControls($("#lstVideo"));
      }
   });
}

function addVideo(){
   $("#btnCancelar").unbind("click");
   $("#btnGravar").unbind("click");
   
   $("#lstVideo").toggleClass("hide");
   $("#dadosVideo").toggleClass("hide");
   $("#btnCancelar").toggleClass("hide");
   $("#btnCancelar").click(function(e){
      $("#lstVideo").toggleClass("hide");
      $("#dadosVideo").toggleClass("hide");
      $("#btnCancelar").toggleClass("hide");
      $("#btnGravar").toggleClass("hide");
      $("#frmUnidade #vNome").val("");
      $("#frmUnidade #vLocal").prop("checked", "checked");
      validarTipoVideo();
      $("#frmUnidade #vFile").val("");
      $("#frmUnidade #vLink").val("");
      $("#frmUnidade #vPlataforma").val("");
   });
   $("#btnGravar").toggleClass("hide");
   $("#btnGravar").click(function(e){
      insVideo();
   });
}

function insVideo() {
   if($("#frmUnidade #vNome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   if($("#frmUnidade #vLocal").prop("checked")){
      if($("#frmVLocal #vFile").val() == ""){
         alert('Selecione o arquivo desejado.');
         return;
      }
   }
   else{
      if($("#frmUnidade #vLink").val() == ""){
         alert('O campo Link deve ser preenchido.');
         return;
      }
      if($("#frmUnidade #vPlataforma").val() == ""){
         alert('Selecione a plataforma correspondente.');
         return;
      }
   }
      
   if($("#frmUnidade #vLocal").prop("checked")){
      $("#frmVLocal #vNome").val($("#frmUnidade #vNome").val());
      $("#frmVLocal").submit();
   } else {
      $.ajax({
         url: "sitAJAX.php",
         data: "funcao=insVideo&unidade=" + $("#frmUnidade #unidade").val() + "&nome=" + $("#frmUnidade #vNome").val() + "&link=" + $("#frmUnidade #vLink").val() + "&plataforma=" + $("#frmUnidade #vPlataforma").val(),
         method: "POST",
         success: function(dados){
            var retorno = JSON.parse(dados);
            if(retorno.retorno != "OK"){
               alert(retorno.retorno + "!\n" + retorno.msg);
               return;
            }
            alert("Vídeo cadastrado.");
            lstVideos();
            $("#btnCancelar").click();
         }
      });
   }
}

function exibirVideo(id){
   var janela, modo;
   
   if(screen.width > 768){
      janela = "height=600,width=780,location=no,toolbar=no,titlebar=no,scrollbars=yes,left=0,top=0";
      modo = "pop";
   } else {
      janela = "";
      modo = "_blank";
   }
   window.open("video.php?id=" + id, modo, janela);
}

function lstAtividades() {
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getAtividades&unidade=" + $("#frmUnidade #unidade").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "";
         for(let atividade of retorno.msg)
            if(typeof atividade === "object")
               html += "<p class='secao-corpo'><em class='glyphicon glyphicon-list-alt'></em> " + atividade.nome + "<a class='pull-right glyphicon glyphicon-edit' data-toggle='tooltip' title='Editar atividade' onclick='editAtividade(\"" + atividade.id + "\");'></a></p>";
            else
               html += "<p class='text-muted'>" + atividade + "</p>";
         $("#lstAtividade").html(html);
         initControls($("#lstAtividade"));
      }
   });
}

function addAtividade(){
   $("#btnCancelar").unbind("click");
   $("#btnGravar").unbind("click");
   
   $("#lstAtividade").toggleClass("hide");
   $("#dadosAtividade").toggleClass("hide");
   $("#btnCancelar").toggleClass("hide");
   $("#btnCancelar").click(function(e){
      $("#lstAtividade").toggleClass("hide");
      $("#dadosAtividade").toggleClass("hide");
      $("#btnCancelar").toggleClass("hide");
      $("#btnGravar").toggleClass("hide");
      $("#frmUnidade #aNome").val("");
   });
   $("#btnGravar").toggleClass("hide");
   $("#btnGravar").click(function(e){
      insAtividade();
   });
}

function insAtividade() {
   if($("#frmUnidade #aNome").val() == ""){
      alert('O campo Nome deve ser preenchido.');
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insAtividade&unidade=" + $("#frmUnidade #unidade").val() + "&nome=" + $("#frmUnidade #aNome").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Atividade cadastrada.");
         $("#frmAtividade #atividade").val(retorno.msg);
         $("#frmAtividade").submit();
      }
   });
}

function editAtividade(id){
   $("#frmAtividade #atividade").val(id);
   $("#frmAtividade").submit();
}

function abrirAtividade(unidade, atividade){
   $("#frmAtividade #unidade").val(unidade);
   $("#frmAtividade #atividade").val(atividade);
   $("#frmAtividade").submit();
}

function chkAtividade(atividade){
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=chkAtividade&atividade=" + atividade,
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno == "Erro"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         else if(retorno.retorno == "OK"){
            for(let qst of retorno.msg){
               let fundo = qst.resp == 0 ? "#D9534F" : "#5CB85C";
               $("#" + qst.id + "_alt" + qst.alt).prop("checked", "checked");
               $("#" + qst.id + "_alt" + qst.alt).closest(".alt").css("background-color", fundo);
            }
            $("#btnGravar").removeClass("btn-success");
            $("#btnGravar").addClass("btn-primary");
            $("#btnGravar").html("Nova tentativa");
            $("#btnGravar").unbind("click");
            $("#btnGravar").click(function(e){
               reabrirAtividade();
            });
            $(".tab-pane input[type=radio]").prop("disabled", "disabled");
         }
         else{
            $("#btnGravar").removeClass("btn-primary");
            $("#btnGravar").addClass("btn-success");
            $("#btnGravar").html("Gravar");
            $("#btnGravar").unbind("click");
            $("#btnGravar").click(function(e){
               gravarAtividade();
            });
            $(".tab-pane input[type=radio]").prop("disabled", "");
         }
      }
   });
   $(".nav.nav-tabs > li:first > a").click();
}

function reabrirAtividade(){
   if(confirm("Ao realizar uma nova tentativa, a anterior será excluída. Deseja prosseguir?")){
      $("#btnGravar").removeClass("btn-primary");
      $("#btnGravar").addClass("btn-success");
      $("#btnGravar").html("Gravar");
      $("#btnGravar").unbind("click");
      $("#btnGravar").click(function(e){
         gravarAtividade();
      });
      $(".alt").css("background-color", "");
      $(".alt:odd").css("background-color", "#cccccc");
      $("input[type=radio]").removeAttr("checked");
      $(".nav.nav-tabs > li:first > a").click();
      $(".tab-pane input[type=radio]").prop("disabled", "");
   }
}

function gravarAtividade(){
   var qtQst, qtResp, respostas;
   
   qtQst = $("div[id^=qst]").length;
   respostas = Array();
   
   $("div[id^=qst]").each(function(){
      if($(this).find("input[type=radio]:checked").length > 0)
         respostas.push($(this).find("input[type=radio]:checked").attr("id"));
   });
   
   qtResp = respostas.length;
   if(qtQst > qtResp){
      alert("É necessário responder todas as questões.");
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=gravaAtividade&curso=" + $("#frmAtividade #curso").val() + "&atividade=" + $("#frmAtividade #atividade").val() + "&dados=" + JSON.stringify(respostas),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Atividade realizada.");
         chkAtividade($("#frmAtividade #atividade").val());
         $(".nav.nav-tabs > li:first > a").click();
      }
   });
}

function lstQuestoes(){
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getQuestoes&unidade=" + $("#frmAtividade #unidade").val() + "&atividade=" + $("#frmAtividade #atividade").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "";
         if(typeof retorno.msg == "object"){
            var i = 1;
            for(let questao of retorno.msg){
               html += "<p class='secao-corpo'><em class='glyphicon glyphicon-list-alt'></em> Questão " + (i < 10 ? ("0" + i) : i) + "<a class='pull-right glyphicon glyphicon-edit' data-toggle='tooltip' title='Editar questão' onclick='editQuestao(\"" + questao + "\");'></a></p>";
               i++;
            }
         } else {
            html += "<p class='text-muted'>" + retorno.msg + "</p>";
         }
         $("#lstQuestao").html(html);
         initControls($("#lstQuestao"));
      }
   });
}

function addQuestao(){
   $("#btnCancelar").unbind("click");
   $("#btnGravar").unbind("click");
   
   $("#lstQuestao").toggleClass("hide");
   $("#dadosQuestao").toggleClass("hide");
   $("#btnCancelar").toggleClass("hide");
   $("#btnCancelar").click(function(e){
      $("#lstQuestao").toggleClass("hide");
      $("#dadosQuestao").toggleClass("hide");
      $("#btnCancelar").toggleClass("hide");
      $("#btnGravar").toggleClass("hide");
      $("#frmAtividade #qID").val("");
      $("#frmAtividade #questao").val("");
      $("#frmAtividade #alt1").val("");
      $("#frmAtividade #alt2").val("");
      $("#frmAtividade #alt3").val("");
      $("#frmAtividade #alt4").val("");
      $("#frmAtividade #resp1").prop("checked", "checked");
   });
   $("#btnGravar").toggleClass("hide");
   $("#btnGravar").click(function(e){
      insQuestao();
   });
}

function insQuestao() {
   if($("#frmAtividade #questao").val() == ""){
      alert('O campo Questão deve ser preenchido.');
      return;
   }
   if($("#frmAtividade #alt1").val() == ""){
      alert('O campo correspondente à alternativa 1 deve ser preenchido.');
      return;
   }
   if($("#frmAtividade #alt2").val() == ""){
      alert('O campo correspondente à alternativa 2 deve ser preenchido.');
      return;
   }
   if($("#frmAtividade #alt3").val() == ""){
      alert('O campo correspondente à alternativa 3 deve ser preenchido.');
      return;
   }
   if($("#frmAtividade #alt4").val() == ""){
      alert('O campo correspondente à alternativa 4 deve ser preenchido.');
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insQuestao&unidade=" + $("#frmAtividade #unidade").val() + "&atividade=" + $("#frmAtividade #atividade").val() + "&qID=" + $("#frmAtividade #qID").val() + "&questao=" + $("#frmAtividade #questao").val() + "&alt1=" + $("#frmAtividade #alt1").val() + "&alt2=" + $("#frmAtividade #alt2").val() + "&alt3=" + $("#frmAtividade #alt3").val() + "&alt4=" + $("#frmAtividade #alt4").val() + "&resp=" + $("#frmAtividade input[name=resp]:checked").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert(retorno.msg);
         lstQuestoes();
         $("#btnCancelar").click();
      }
   });
}

function editQuestao(id) {
   if(id == ""){
      alert("Questão inválida.");
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getQuestao&atividade=" + $("#frmAtividade #atividade").val() + "&qID=" + id,
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var qst = retorno.msg;
         addQuestao();
         $("#frmAtividade #qID").val(id);
         $("#frmAtividade #questao").val(qst.questao);
         $("#frmAtividade #alt1").val(qst.alt1);
         $("#frmAtividade #alt2").val(qst.alt2);
         $("#frmAtividade #alt3").val(qst.alt3);
         $("#frmAtividade #alt4").val(qst.alt4);
         $("#frmAtividade #resp" + qst.resp).prop("checked", "checked");
      }
   });
}

function chkAvaliacao(){
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=chkAvaliacao&curso=" + $("#frmAval #curso").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno == "Erro"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         else if(retorno.retorno == "OK"){
            var aval = retorno.msg;
            $("#frmAval #nota" + aval.nota).prop("checked", "checked");
            $("#frmAval #comentario").val(aval.comentario);
         }
      }
   });
}

function validarAvaliacao(){
   if($("#frmAval input[name=nota]:checked").length == 0){
      alert("Informe a nota para prosseguir.");
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insAvaliacao&curso=" + $("#frmAval #curso").val() + "&nota=" + $("#frmAval input[name=nota]:checked").val() + "&comentario=" + $("#frmAval #comentario").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Avaliação realizada.\nAgradecemos sua opnião.");
         $("#btnVoltar").click();
      }
   });
}

function lstTopicos(){
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getForuns&curso=" + $("#frmForum #curso").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "<table class='table table-bordered table-striped table-hover table-responsive'><thead><tr><th>Tópico</th><th>Autor</th><th>Última atualização</th></tr></thead><tbody>";
         var foruns = retorno.msg;
         if(foruns.length > 0)
            for(let forum of foruns)
               html += "<tr><td><a onclick='abrirTopico(\"" + forum.forum + "\")'>" + forum.topico + "</a></td><td>" + forum.autor + "</td><td>" + forum.data + "</td></tr>";
         else
            html += "<tr><td colspan=3>Nenhum tópico cadastrado</td></tr>";
         html += "</tbody></table>";
         $("#lstTopico").html(html);
         initControls($("#lstTopico"));
      }
   });
}

function addTopico(){
   $("#lstTopico").toggleClass("hide");
   $("#dadosTopico").toggleClass("hide");
   $(".panel-footer").toggleClass("hide");
}

function abrirTopico(topico){
   $("#frmTopico #topico").val(topico);
   $("#frmTopico").submit();
}

function validarForum(){
   if($("#frmForum #topico").val() == ""){
      alert("O campo Tópico deve ser preenchido.");
      return;
   }
   if($("#frmForum #msg").val() == ""){
      alert("O campo Mensagem deve ser preenchido.");
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insTopico&curso=" + $("#frmForum #curso").val() + "&topico=" + $("#frmForum #topico").val() + "&msg=" + $("#frmForum #msg").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Tópico cadastrado.");
         $("#frmTopico #topico").val(retorno.msg);
         $("#frmTopico").submit();
      }
   });
}

function lstMensagens(topico){
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=getMensagens&topico=" + topico,
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "";
         var mensagens = retorno.msg;
         if(mensagens.length > 0)
            for(let msg of retorno.msg)
               html += "<div class='panel panel-default'><div class='panel-body'><small class='pull-right'>" + msg.autor + " [" + msg.data + "]</small><p class='secao-corpo'>" + msg.msg + "</p></div></div>";
         $("#lstMsg").html(html);
         initControls($("#lstMsg"));
      }
   });
}

function validarMsg(){
   if($("#frmMsg #msg").val() == ""){
      alert("O campo Mensagem deve ser preenchido.");
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=insMensagem&topico=" + $("#frmMsg #topico").val() + "&msg=" + $("#frmMsg #msg").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         alert("Mensagem cadastrada.");
         lstMensagens($("#frmMsg #topico").val());
         $("#btnCancelar").click();
      }
   });
}

function relUsuario(){
   if($("#frmUsr #usr").val() == ""){
      alert('Selecione o Usuário.');
      return;
   }
   $.ajax({
      url: "sitAJAX.php",
      data: "funcao=relatorio&uid=" + $("#frmUsr #usr").val(),
      method: "POST",
      success: function(dados){
         var retorno = JSON.parse(dados);
         if(retorno.retorno != "OK"){
            alert(retorno.retorno + "!\n" + retorno.msg);
            return;
         }
         var html = "<table class='table table-bordered table-striped table-hover table-responsive'><thead><tr><th>Curso</th><th>Vídeos assistidos (%)</th><th>Nota</th><th>Aprovação</th></tr></thead><tbody>";
         var results = retorno.msg;
         if(results.length > 0)
            for(let result of results){
               let icon = result.aprov == 1 ? "ok" : "remove";
               let color = result.aprov == 1 ? " text-success" : " text-danger";
               html += "<tr><td>" + result.curso + "</a></td><td class='text-center'>" + result.videos + "</td><td class='text-center'>" + result.nota + "</td><td class='text-center" + color + "'><em class='glyphicon glyphicon-" + icon + "'></em></td></tr>";
            }
         else
            html += "<tr><td colspan='4'>Nenhum resultado para a pesquisa</td></tr>";
         html += "</tbody></table>";
         $(".row.hide").removeClass("hide");
         $("#resultado").html(html);
         initControls($("#resultado"));
      }
   });
}
///////////////////////////////////////////////////////////////////