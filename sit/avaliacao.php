<?php

if($usuario_logado->nivel == 0)
   erro("Você não tem autorização para acessar esta página.");

if($usuario_logado->nivel == 1 && !cursoUsuario($curso, $usuario_logado->uid))
   erro("Você não está inscrito neste curso.", "cursos");

?>

<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="" id="frmAval" name="frmAval">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="nota">Nota <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <div class="col-xs-12 text-center">
                     <div class="col-xs-2">
                        <p class="small">1</p>
                        <input class="simple" type="radio" id="nota1" name="nota" value="1" />
                        <p class="small">Muito insatisfeito</p>
                     </div>
                     <div class="col-xs-2">
                        <p class="small">2</p>
                        <input class="simple" type="radio" id="nota2" name="nota" value="2" />
                     </div>
                     <div class="col-xs-2">
                        <p class="small">3</p>
                        <input class="simple" type="radio" id="nota3" name="nota" value="3" />
                     </div>
                     <div class="col-xs-2">
                        <p class="small">4</p>
                        <input class="simple" type="radio" id="nota4" name="nota" value="4" />
                     </div>
                     <div class="col-xs-2">
                        <p class="small">5</p>
                        <input class="simple" type="radio" id="nota5" name="nota" value="5" />
                        <p class="small">Muito satisfeito</p>
                     </div>
                  </div>                  
               </div>
               <div class="form-group col-xs-12">
                  <label for="comentario">Comentário</label>
                  <textarea class="form-control" rows="4" id="comentario" name="comentario" value=""></textarea>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" id="btnVoltar" onclick="$('#frmCurso').submit();">Voltar</button>
            <button type="button" class="btn btn-success" onclick="validarAvaliacao();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $curso; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= get_nomeCurso($curso) . " - Avaliação"; ?>");
      chkAvaliacao();
   });
</script>