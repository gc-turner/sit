<?php

$cursos = array();

$sql = "SELECT id, nome, categoria, pchave, instrutor FROM curso WHERE id = '$id'";

$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
if(!$linha = $result->fetch())
   erro("Curso não encontrado.", "cursos");

$Cid = $linha['id'];
$Cnome = $linha['nome'];
$Ccategoria = $linha['categoria'];
$Cpchave = isset($linha['pchave']) ? $linha['pchave'] : "";
$Cinstrutor = $linha['instrutor'];

?>

<div class="row"> 
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <div class="panel panel-default">
         <div class="panel-heading">
            <button type="button" class="btn btn-warning" onclick="direciona('cursos');">Voltar</button>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <p class="secao-titulo">Sobre o curso</p>
                  <dl class="dl-horizontal">
                     <dt>Instrutor: </dt>
                     <dd><?= $Cinstrutor; ?></dd>
                     <dt>Categoria: </dt>
                     <dd><?= $Ccategoria; ?></dd>
                     <?php if($Cpchave != "") { ?>
                     <dt>Palavras-chave:</dt>
                     <?php $arrPchave = explode(". ", $Cpchave);
                     foreach($arrPchave as $value)
                        echo "<dd>" . str_replace(".", "", $value) . "</dd>";
                     } ?>
                  </dl>
               <?php
               include($usuario_logado->nivel == 0 ? "curso_adm.php" : "curso_usr.php");
               ?>
            </div>
         </div>
      </div>
   </div>
</div>

<form role="form" id="frmForum" name="frmForum" method="post" action="./">
   <input type="hidden" id="page" name="page" value="forum" />
   <input type="hidden" id="curso" name="curso" value="<?= $Cid; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Curso: <?= $Cnome; ?>");
   });
</script>