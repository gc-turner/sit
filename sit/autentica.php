<?php
require_once("bd.php");

session_start();

if(isset($_SESSION['usuario_logado']))
   header("Location: ./");

$arr = (array_merge($_POST, $_GET));
foreach($arr as $key => $value){
   eval("$" . $key . " = \"" . addslashes(str_replace("'", "", $value)) . "\";");
}

$sql = "SELECT uid, uname, upass, nome, nivel, excluido FROM usuario WHERE uname = '$uname'";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);

$msg = "";

if($linha = $result->fetch()){   
   $auth = true;
   if($linha['excluido'] == '1'){
      $msg = "Login desabilitado.";
      $pagina = "login.php";
      $auth = false;
   }
   if($auth && !password_verify($upass, trim($linha['upass']))){
      $msg = "Senha inválida.";
      $pagina = "login.php";
      $auth = false;
   }
   if($auth){
      
      if (!isset($usuario)) 
         $usuario = new stdClass();
      $usuario->uid = $linha['uid'];
      $usuario->uname = $linha['uname'];
      $usuario->nome = $linha['nome'];
      $usuario->nivel = intval($linha['nivel']);
      
      $_SESSION['usuario_logado'] = $usuario;
      
      $pagina = "./";
   }
}
else{
   $msg = "Usuário não cadastrado.";
   $pagina = "login.php";
}

?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8" />
      <title>Sistema Interno de Treinamentos</title>
      
      <link rel="icon" href="img/favicon.ico" />
   </head>
   <body>
      <script>
         <?php if($msg != "") { ?>
            alert("<?= $msg; ?>");
         <?php } ?>
         window.location = "<?= $pagina; ?>";
      </script>
   </body>
</html>