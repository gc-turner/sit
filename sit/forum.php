<?php

if($usuario_logado->nivel == 1 && !cursoUsuario($curso, $usuario_logado->uid))
   erro("Você não está inscrito neste curso.", "cursos");

?>

<div class="row">
   <div class="col-xs-12 col-md-8 col-md-offset-2">
      <form role="form" class="panel panel-default" method="post" action="" id="frmForum" name="frmForum">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <div class="panel-heading">
            <button type="button" class="btn btn-warning" id="btnVoltar" onclick="$('#frmCurso').submit();">Voltar</button>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-xs-12">
                  <p class="secao-titulo">
                     Tópicos
                     <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Novo tópico" onclick="addTopico();"></a>
                  </p>
                  <div id="lstTopico"></div>
                  <div id="dadosTopico" class="hide">
                     <div class="form-group col-xs-12">
                        <label for="topico">Tópico <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="150" id="topico" name="topico" value="" />
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="msg">Mensagem <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="msg" name="msg" value=""></textarea>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-footer hide">
            <button type="button" class="btn btn-warning" id="btnCancelar">Cancelar</button>
            <button type="button" class="btn btn-success" id="btnGravar" onclick="validarForum();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $curso; ?>" />
</form>

<form role="form" id="frmTopico" name="frmTopico" method="post" action="./">
   <input type="hidden" id="page" name="page" value="topico" />
   <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
   <input type="hidden" id="topico" name="topico" value="" />
</form>
   
<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= get_nomeCurso($curso) . " - Fórum"; ?>");
      lstTopicos();
      $("#btnCancelar").unbind("click");
      $("#btnCancelar").click(function(e){
         $("#lstTopico").toggleClass("hide");
         $("#dadosTopico").toggleClass("hide");
         $(".panel-footer").toggleClass("hide");
         $("#frmForum #topico").val("");
         $("#frmForum #msg").val("");
      });
   });
</script>