<div class="row">
   <div class="col-xs-12 col-md-4 col-md-offset-4">
      <form role="form" class="panel panel-default" method="post" action="" id="frmPass" name="frmPass">
         <input type="hidden" id="uid" name="uid" value="<?= $usuario_logado->uid; ?>" />
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="opass">Senha atual <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="password" maxlength="15" id="opass" name="opass" value="" />
               </div>
               <div class="form-group col-xs-12">
                  <label for="npass">Nova senha <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="password" maxlength="15" id="npass" name="npass" value="" />
               </div>
               <div class="form-group col-xs-12">
                  <label for="cpass">Confirmação de Senha <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="password" maxlength="15" id="cpass" name="cpass" value="" />
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" onclick="direciona('inicio');">Voltar</button>
            <button type="button" class="btn btn-success" onclick="alterarSenha();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Alteração de senha");
   });
</script>