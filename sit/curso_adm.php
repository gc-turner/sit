<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

?>
   <p class="secao-corpo">
      <em class="glyphicon glyphicon-comment"></em> <a onclick="$('#frmForum').submit();">Fórum</a>
   </p>
</div>
<div class="form-group col-xs-12">
   <p class="secao-titulo">
      Material&nbsp;
      <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Adicionar material" onclick="editCurso('material');"></a>
   </p>
   <!-- Materiais do curso -->
   <?php
   $sql = "SELECT id, nome FROM curso_material WHERE id_curso = '$Cid' ORDER BY id";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   if($linha = $result->fetch()){
      while($linha){
         $file = base64_encode($Cid . "_" .$linha['id'] . "_" . time());
         echo "<p class='secao-corpo'>
               <em class='glyphicon glyphicon-file'></em>&nbsp;
               <a onclick='window.open(\"download.php?id=$file\");'>
               {$linha['nome']}
               </a>
            </p>";
         $linha = $result->fetch();
      }
   } else {
      echo "<p class='text-muted'>Nenhum material cadastrado</p>";
   }
   ?>
</div>
<div class="form-group col-xs-12">
   <p class="secao-titulo">
      Unidades&nbsp;
      <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Nova unidade" onclick="editCurso('unidade');"></a>
   </p>
   <!-- Unidades do curso -->
   <?php
   $sql = "SELECT id, nome FROM curso_unidade WHERE id_curso = '$Cid' ORDER BY id";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   if($linha = $result->fetch()){
      while($linha){
         $hr = "";
         echo "<p class='secao-titulo2'>
            <em class='glyphicon glyphicon-asterisk'></em>&nbsp;
            {$linha['nome']}
            <a class='pull-right glyphicon glyphicon-edit' data-toggle='tooltip' title='Editar conteúdo' onclick='editCurso(\"unidade\", \"{$linha['id']}\");'></a>
         </p>";
         //Videos
         $sql = "SELECT nome FROM curso_video WHERE id_unidade = '{$linha['id']}' ORDER BY id";
         $rItem = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
         if($lItem = $rItem->fetch()){
            $hr = "<hr />";
            while($lItem){
               echo "<p class='secao-corpo'><em class='glyphicon glyphicon-film'></em> {$lItem['nome']}</p>";
               $lItem = $rItem->fetch();
            }
         }
         //Atividades
         $sql = "SELECT nome FROM curso_atividade WHERE id_unidade = '{$linha['id']}' ORDER BY id";
         $rItem = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
         if($lItem = $rItem->fetch()){
            echo $hr;
            while($lItem){
               echo "<p class='secao-corpo'><em class='glyphicon glyphicon-list-alt'></em> {$lItem['nome']}</p>";
               $lItem = $rItem->fetch();
            }
         }
         
         $linha = $result->fetch();
      }
   } else {
      echo "<p class='text-muted'>Nenhuma unidade cadastrada</p>";
   }
   ?>
</div>
<form role="form" id="frmAcao" name="frmAcao" method="post" action="./">
   <input type="hidden" id="page" name="page" value="" />
   <input type="hidden" id="curso" name="curso" value="<?= $Cid; ?>" />
   <input type="hidden" id="unidade" name="unidade" value="" />
</form>