<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$nomeCurso = get_nomeCurso($curso);

?>

<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="./" id="frmUnidade" name="frmUnidade">
         <input type="hidden" id="page" name="page" value="unidade" />
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
         <?php if($unidade != "") { ?>
         <div class="panel-heading">
            <h3 class="panel-header"><?= get_unidadeCurso($curso, $unidade); ?></h3>
         </div>
         <?php } ?>
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12" id="dadosUnidade">
                  <label for="nome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="nome" name="nome" value="" />
               </div>
               <?php if($unidade != "") { ?>
               <div class="col-xs-12">
                  <p class="secao-titulo">
                     Vídeos&nbsp;
                     <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Novo vídeo" onclick="addVideo();"></a>
                  </p>
                  <div id="lstVideo"></div>
                  <div id="dadosVideo" class="hide">
                     <div class="form-group col-xs-12">
                        <label for="vNome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="60" id="vNome" name="vNome" value="" />
                     </div>
                     <div class="form-group col-xs-12">
                        <div class="radio">
                           <div class="col-xs-6 col-md-4">
                              <input class="simple" type="radio" id="vLocal" name="vTipo" value="0" onchange="validarTipoVideo();" />&nbsp;Local
                           </div>
                           <div class="col-xs-6 col-md-4">
                              <input class="simple" type="radio" id="vExterno" name="vTipo" value="1" onchange="validarTipoVideo();" />&nbsp;Externo
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-xs-12 divLocal">
                        <button type="button" class="btn btn-primary" onclick="$('#frmVLocal #vFile').click();">Selecionar</button>
                     </div>
                     <div class="form-group col-xs-12 col-md-8 divExterno">
                        <label for="vLink">Link <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="60" id="vLink" name="vLink" value="" />
                     </div>
                     <div class="form-group col-xs-12 col-md-4 divExterno">
                        <label for="vPlataforma">Plataforma <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <select class="form-control" id="vPlataforma" name="vPlataforma">
                           <option value="" selected>Selecione</option>
                           <option value="youtube">Youtube</option>
                           <option value="vimeo">Vimeo</option>
                           <option value="metacafe">Metacafe</option>
                           <option value="veoh">Veoh</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-xs-12">
                  <p class="secao-titulo">
                     Atividades&nbsp;
                     <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Nova atividade" onclick="addAtividade();"></a>
                  </p>
                  <div id="lstAtividade"></div>
                  <div id="dadosAtividade" class="hide">
                     <div class="form-group col-xs-12">
                        <label for="aNome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <input class="form-control" type="text" maxlength="50" id="aNome" name="aNome" value="" />
                     </div>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" id="btnVoltar" onclick="$('#frmCurso').submit();">Voltar</button>
            <button type="button" class="btn btn-warning" id="btnCancelar">Cancelar</button>
            <button type="button" class="btn btn-success" id="btnGravar">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="<?= $curso; ?>" />
</form>

<form role="form" method="post" action="video_ins.php" id="frmVLocal" name="frmVLocal" enctype="multipart/form-data">
   <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
   <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
   <input type="hidden" id="vNome" name="vNome" value="" />
   <input type="file" accept=".mp4" class="hide" id="vFile" name="vFile" />
</form>

<form role="form" method="post" action="./" id="frmAtividade" name="frmAtividade">
   <input type="hidden" id="page" name="page" value="atividade_cad" />
   <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
   <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
   <input type="hidden" id="atividade" name="atividade" value="" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= $nomeCurso; ?> - Unidade");
      $("#btnCancelar").toggleClass("hide");
      $("#btnGravar").toggleClass("hide");
      $("#frmUnidade #vLocal").prop("checked", "checked");
      validarTipoVideo();
      <?php if($unidade == "") { ?>
      $("#btnGravar").toggleClass("hide");
      $("#btnGravar").click(function(e){
         insUnidade();
      });
      <?php } else { ?>
      $("#dadosUnidade").hide();
      lstVideos();
      lstAtividades();
      <?php } ?>
   });
</script>