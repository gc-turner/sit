<?php
require_once('cabecalho.php');

?>

   </head>
   <body class="fixed">
      <form role="form" id="frmPage" name="frmPage" action="./" method="post">
         <input type="hidden" id="page" name="page" value="<?= $page; ?>" />
      </form>
      <header>
         <figure><img class="icon" src="img/logo-light.png" /></figure>
         <nav>
            <a href="#" onclick="hideMenu();"><em class="glyphicon glyphicon-menu-hamburger"></em></a>
            <h1 class="main-title">Sistema Interno de Treinamentos</h1>
            <a class="pull-right" onclick="window.location = 'logout.php';" data-toggle="tooltip" title="Sair"><em class="glyphicon glyphicon-log-out"></em></a>
         </nav>
      </header>
      <aside class="side-menu">
         <p class="text-bold text-center"><?= $usuario_logado->nome; ?></p>
         <nav>
            <ul>
               <li><a onclick="direciona('inicio');"><em class="glyphicon glyphicon-home"></em> Início</a></li>
               <li><a onclick="direciona('alt_senha');"><em class="glyphicon glyphicon-lock"></em> Alterar senha</a></li>
               <li><a onclick="direciona('cursos');"><em class="glyphicon glyphicon-education"></em> Cursos</a></li>
               <?php if($usuario_logado->nivel == 0) { ?>
               <li><a onclick="direciona('usuario');"><em class="glyphicon glyphicon-user"></em> Usuários</a></li>
               <li><a onclick="direciona('relatorio');"><em class="glyphicon glyphicon-stats"></em> Relatório</a></li>
               <?php } ?>
               <li><a onclick="window.location = 'logout.php';"><em class="glyphicon glyphicon-log-out"></em> Sair</a></li>
            </ul>
         </nav>
      </aside>
      <aside class="content">
            <section class="page-title col-xs-12">
               <h2 class="title text-primary">&nbsp;</h2>
            </section>
            <section class="page-content">
               <?php
               if (file_exists("./$page.php"))
                  include("$page.php");
               else
                  erro("A página informada não existe.");
               ?>
            </section>
      </aside>
      
      <?php
         $bd = null;
      ?>
   </body>
</html>