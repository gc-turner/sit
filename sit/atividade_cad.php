<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

?>

<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="" id="frmAtividade" name="frmAtividade">
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
         <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
         <input type="hidden" id="atividade" name="atividade" value="<?= $atividade; ?>" />
         <div class="panel-heading">
            <h3 class="panel-header"><?= get_atividadeUnidade($unidade, $atividade); ?></h3>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-xs-12">
                  <p class="secao-titulo">
                     Questões&nbsp;
                     <a class="pull-right glyphicon glyphicon-plus" data-toggle="tooltip" title="Nova questão" onclick="addQuestao();"></a>
                  </p>
                  <div id="lstQuestao"></div>
                  <div id="dadosQuestao" class="hide">
                     <input type="hidden" id="qID" name="qID" value="" />
                     <div class="form-group col-xs-12">
                        <label for="questao">Questão <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="questao" name="questao" value=""></textarea>
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="alt1">a&#41; <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="alt1" name="alt1" value=""></textarea>
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="alt2">b&#41; <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="alt2" name="alt2" value=""></textarea>
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="alt3">c&#41; <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="alt3" name="alt3" value=""></textarea>
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="alt4">d&#41; <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                        <textarea class="form-control" rows="4" id="alt4" name="alt4" value=""></textarea>
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="resp">Resposta</label>
                        <div class="radio">
                           <div class="col-xs-6 col-lg-3">
                              <input class="simple" type="radio" id="resp1" name="resp" value="1" />&nbsp;A
                           </div>
                           <div class="col-xs-6 col-lg-3">
                              <input class="simple" type="radio" id="resp2" name="resp" value="2" />&nbsp;B
                           </div>
                           <div class="col-xs-6 col-lg-3">
                              <input class="simple" type="radio" id="resp3" name="resp" value="3" />&nbsp;C
                           </div>
                           <div class="col-xs-6 col-lg-3">
                              <input class="simple" type="radio" id="resp4" name="resp" value="4" />&nbsp;D
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" id="btnVoltar" onclick="$('#frmUnidade').submit();">Voltar</button>
            <button type="button" class="btn btn-warning" id="btnCancelar">Cancelar</button>
            <button type="button" class="btn btn-success" id="btnGravar">Gravar</button>
         </div>
      </form>
   </div>
</div>

<form role="form" method="post" action="./" id="frmUnidade" name="frmUnidade">
   <input type="hidden" id="page" name="page" value="unidade" />
   <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
   <input type="hidden" id="unidade" name="unidade" value="<?= $unidade; ?>" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("<?= get_nomeCurso($curso) . " - " . get_unidadeCurso($curso, $unidade); ?>");
      $("#btnCancelar").toggleClass("hide");
      $("#btnGravar").toggleClass("hide");
      $("#frmAtividade #resp1").prop("checked", "checked");
      lstQuestoes();
   });
</script>