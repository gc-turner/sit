<?php

session_start();

if(isset($_SESSION['usuario_logado']))
   header("Location: ./");
?>

<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8" />
      <title>Sistema Interno de Treinamentos</title>
      
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      
      <link rel="icon" href="img/favicon.ico" />
      
      <!-- CSS -->
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />

      <!-- Optional theme -->
      <link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css" />
      
      <link rel="stylesheet" href="css/sit.css" />
      
      <!-- Javascript -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/jquery/jquery-2.2.4.min.js"></script>

      <!-- Latest compiled and minified JavaScript -->
      <script src="js/bootstrap/bootstrap.min.js"></script>
   </head>
   <body class="container">
      <div class="row">
         <div class="col-xs-12 col-md-4 col-md-offset-4 login-panel">
            <form role="form" id="frmLogin" name="frmLogin" class="panel panel-default" method="post" action="autentica.php">
               <div class="panel-body">
                  <div class="row">
                     <div class="form-group col-xs-12 text-center">
                        <img alt="logo" src="img/lglogo-dark.png" />
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="uid">Usuário</label>
                        <input type="text" class="form-control" id="uname" name="uname" maxlength="10" value="" />
                     </div>
                     <div class="form-group col-xs-12">
                        <label for="upass">Senha</label>
                        <input type="password" class="form-control" id="upass" name="upass" maxlength="15" value="" />
                     </div>
                  </div>
               </div>
               <div class="panel-footer">
                  <button type="submit" class="btn btn-success">Autenticar</button>
               </div>
            </form>
         </div>
      </div>
   </body>
</html>