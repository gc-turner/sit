<?php

$cursos = array();

$sql = "SELECT id, nome, categoria FROM curso ORDER BY nome";

$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
while($linha = $result->fetch()){
   $cursos[$linha['id']] = array("nome" => $linha['nome'], "categoria" => $linha['categoria']);
}

?>

<div class="row"> 
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <div class="panel panel-default">
         <div class="panel-heading">
            <button type="button" class="btn btn-warning" onclick="direciona('inicio');">Voltar</button>
            <?php if($usuario_logado-> nivel == 0) { ?>
            <button type="button" class="btn btn-success" onclick="direciona('curso_novo');">Adicionar</button>
            <?php } ?>
         </div>
         <div class="panel-body">
            <div class="row">
               <?php
               if(count($cursos) > 0) {
                  foreach($cursos as $key => $value){ 
                     $txtBold = cursoUsuario($key, $usuario_logado->uid) ? " text-bold" : "";
                  ?>
                  <p class="reg-info col-xs-12<?= $txtBold; ?>">
                     <a onclick="abrirCurso('<?= $key; ?>');">
                     <?= "{$value['nome']} <em class='glyphicon glyphicon-info-sign' data-toggle='tooltip' title='Categoria: {$value['categoria']}'></em>"; ?> 
                     </a>
                  </p>
               <?php 
                     } 
               } else { ?>
               <p class="empty-info">Nenhum curso encontrado!</p>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>

<form role="form" id="frmCurso" name="frmCurso" method="post" action="./">
   <input type="hidden" id="page" name="page" value="curso" />
   <input type="hidden" id="id" name="id" value="" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Cursos");
      $(".reg-info:odd").css("background-color", "#cccccc");
   });
</script>