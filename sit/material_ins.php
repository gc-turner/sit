<?php
require_once("cabecalho.php");

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

if($arquivo['tmp_name'] != "" && ($arquivo['size'] > 0 && $arquivo['size'] <= (1024 * 1024 * 5))){ //5MB
   $arq = explode(".", $arquivo['name']);
   $arq_tipo = $arq[count($arq) - 1];
   
   $arq = fopen($arquivo['tmp_name'], "rb");
   $arq_bin = fread($arq, $arquivo['size']);
   fclose($arq);
   $arq_bin = addslashes($arq_bin);
   
   $sql = "INSERT INTO curso_material (id_curso, nome, arquivo, tipo) VALUES ('$curso', '$nome', '$arq_bin', '$arq_tipo')";

   $bd->query($sql) or die("Erro ao acessar a base de dados. Erro: " . $bd->errorInfo()[2]);
   
   $msg = "Material inserido.";
}
else {
   $msg = "Erro:\\nArquivo inválido!";
}

?>
   </head>
   <body>
      <form role="form" id="frmMaterial" name="frmMaterial" method="post" action="./">
         <input type="hidden" id="page" name="page" value="material" />
         <input type="hidden" id="curso" name="curso" value="<?= $curso; ?>" />
      </form>
      
      <script>         
         alert("<?= $msg; ?>");
         $("#frmMaterial").submit();
      </script>
      <?php
      $bd = null;
      ?>
   </body>
</html>