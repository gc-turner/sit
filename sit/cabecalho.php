<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8" />
      <title>Sistema Interno de Treinamentos</title>
      
      <meta name="keywords" content="SIT, Sistema Interno de Treinamentos, Treinamentos, Desenvolvimento Aplicações Web" />
      
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      
      <link rel="icon" href="img/favicon.ico" />
      
      <!-- CSS -->
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />

      <!-- Optional theme -->
      <link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css" />
      
      <link rel="stylesheet" href="css/sit.css" />
      
      <!-- Javascript -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/jquery/jquery-2.2.4.min.js"></script>

      <!-- Latest compiled and minified JavaScript -->
      <script src="js/bootstrap/bootstrap.min.js"></script>
      
      <script src="js/sit.js"></script>
      
      <?php
      require_once("config.php");
      ?>
