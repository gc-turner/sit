<?php

if($usuario_logado->nivel == 1)
   erro("Você não tem autorização para acessar esta página.");

$usuarios = array();
$sql = "SELECT uid, uname, nome FROM usuario WHERE excluido = '0' ORDER BY nome";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
while($linha = $result->fetch()){
   $usuarios[$linha['uid']] = array("nome" => $linha['nome'], "uname" => $linha['uname']);
}

if(isset($uid) && $uid != NULL){
   $sql = "SELECT uname, nome, nivel FROM usuario WHERE uid = '$uid'";
   $result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
   $linha = $result->fetch();
   $nome = $linha['nome'];
   $uname = $linha['uname'];
   $nivel = $linha['nivel'];
}

?>
<div class="row">
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <form role="form" class="panel panel-default" method="post" action="usuario_ins.php" id="frmUsr" name="frmUsr">
         <input type="hidden" id="uid" name="uid" value="<?= $uid; ?>" />
         <div class="panel-body">
            <div class="row">
               <div class="form-group col-xs-12">
                  <label for="nome">Nome <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="50" id="nome" name="nome" value="<?= $nome; ?>" />
               </div>
               <div class="form-group col-xs-12 col-md-6">
                  <label for="uname">Usuário <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="text" maxlength="10" id="uname" name="uname" value="<?= $uname; ?>"<?= (isset($uid) && $uid != NULL) ? " disabled" : ""; ?> />
               </div>
               <div class="form-group col-xs-12 col-md-6">
                  <label for="nivel">Nível <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <select class="form-control" id="nivel" name="nivel">
                     <option value="" selected>Selecione</option>
                     <option value="0"<?= ($nivel == '0') ? " selected" : ""; ?>>Administrador</option>
                     <option value="1"<?= ($nivel == '1') ? " selected" : ""; ?>>Usuário</option>
                  </select>
               </div>
               <div class="form-group col-xs-12 col-md-4">
                  <label for="upass">Senha <em data-toggle="tooltip" title="Obrigatório">*</em></label>
                  <input class="form-control" type="password" maxlength="15" id="upass" name="upass" value="" />
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <button type="button" class="btn btn-warning" onclick="direciona('inicio');">Voltar</button>
            <button type="button" class="btn btn-success" onclick="validarUsuario();">Gravar</button>
         </div>
      </form>
   </div>
</div>

<div class="row"> 
   <div class="col-xs-12 col-md-6 col-md-offset-3">
      <div class="panel panel-info">
         <div class="panel-heading">
            <h3 class="panel-header">Usuários cadastrados</h3>
         </div>
         <div class="panel-body">
            <div class="row">
               <?php
               if(count($usuarios) > 0) {
                  foreach($usuarios as $key => $value){ ?>
                  <p class="reg-info col-xs-12">
                     <?= "{$value['nome']} ({$value['uname']})"; ?> 
                     <button class="pull-right btn btn-xs btn-warning" data-toggle="tooltip" title="Alterar usuário: <?= $value['uname']; ?>" onclick="alterarUsuario('<?= $key; ?>');"><em class="glyphicon glyphicon-edit"></em></button>
                     <?php if($key != $usuario_logado->uid){ ?>
                     <button class="pull-right btn btn-xs btn-danger" data-toggle="tooltip" title="Remover usuário: <?= $value['uname']; ?>" onclick="excluirUsuario('<?= $key; ?>');"><em class="glyphicon glyphicon-trash"></em></button>
                     <?php } ?>
                  </p>
               <?php } 
               } else { ?>
               <p class="empty-info">Nenhum usuário cadastrado!</p>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>

<form role="form" id="frmUsrCh" name="frmUsrCh" method="post" action="./">
   <input type="hidden" id="page" name="page" value="usuario" />
   <input type="hidden" id="uid" name="uid" value="" />
</form>

<script>
   $(document).ready(function(){
      $(".page-title > .title").html("Cadastro de Usuários");
      $(".reg-info:odd").css("background-color", "#cccccc");
   });
</script>