<?php
require_once("cabecalho.php");

$vid = base64_decode($id);
$vid = explode("_", $vid);
$id = $vid[0];


$sql = "SELECT * FROM curso_video WHERE id = '$id'";
$result = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);

$sql = "SELECT C.id FROM (curso AS C INNER JOIN curso_unidade AS CU ON C.id = CU.id_curso) INNER JOIN curso_video AS CV ON CU.id = CV.id_unidade WHERE CV.id = '$id'";
$rCurso = $bd->query($sql) or die("Erro ao consultar a base de dados. Erro: " . $bd->errorInfo()[2]);
$lCurso = $rCurso->fetch();

$plataforma['youtube'] = "https://www.youtube.com/embed/@@##@@";
$plataforma['vimeo'] = "https://player.vimeo.com/video/@@##@@";
$plataforma['metacafe'] = "http://www.metacafe.com/embed/@@##@@";
$plataforma['veoh'] = "http://www.veoh.com/swf/webplayer/WebPlayer.swf?version=AFrontend.5.7.0.1509&permalinkId=@@##@@&player=videodetailsembedded&videoAutoPlay=0&id=anonymous";

?>
   </head>
   <body>
      <div class="row">
         <div class="col-xs-12">
            <div class="form-group col-xs-12">
               <?php if($usuario_logado->nivel == 1 && !cursoUsuario($lCurso[0], $usuario_logado->uid)) { ?>
               <p class="reg-info">Você não está inscrito no curso <?= get_nomeCurso($lCurso['id']); ?> ao qual este vídeo pertence.<br />Para assistí-lo, você deve se inscrever.</p>
               <?php  } else if(!$linha = $result->fetch()) { ?>
                  <p class="reg-info">Não foi possível recuperar o vídeo.</p>
               <?php } else {
                  if($usuario_logado->nivel == 1)
                     gravaUsrVideo($usuario_logado->uid, $id, $lCurso[0]);
                  echo "<p class='secao-titulo'>{$linha['nome']}</p>";
                  if($linha['externo'] == '0') {
                     echo "<video width='640px' height='480px' controls src='videos/" . base64_encode($linha['id'] . $linha['nome'] . $lCurso[0] . $linha['id_unidade']) . ".mp4'></video>";
                  } else {
                     echo "<iframe width='640px' height='480px' allowfullscreen src='" . str_replace("@@##@@", $linha['link'], $plataforma[$linha['plataforma']]) . "'></iframe>";
                  }
               } ?>
            </div>
            <div class="form-group col-xs-12">
               <button type="button" class="btn btn-warning" onclick="window.close();">Fechar</button>
            </div>
         </div>
      </div>
      <?php
      $bd = null;
      ?>
   </body>
</html>